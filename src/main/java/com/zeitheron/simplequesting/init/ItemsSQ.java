package com.zeitheron.simplequesting.init;

import com.zeitheron.simplequesting.items.ItemQuestingBook;

public class ItemsSQ
{
	public static final ItemQuestingBook QUESTING_BOOK = new ItemQuestingBook("questing_book", 0x666688);
	public static final ItemQuestingBook EDITOR_BOOK = new ItemQuestingBook("editor_book", 0xFF8888);
}