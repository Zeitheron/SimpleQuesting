package com.zeitheron.simplequesting.net;

import com.pengu.hammercore.net.packetAPI.iPacket;
import com.pengu.hammercore.net.packetAPI.iPacketListener;
import com.zeitheron.simplequesting.SimpleQuesting;
import com.zeitheron.simplequesting.book.Navigation;

import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PacketNavigateQBook implements iPacket, iPacketListener<PacketNavigateQBook, iPacket>
{
	Navigation nav;
	
	public PacketNavigateQBook(Navigation nav)
	{
		this.nav = nav;
	}
	
	public PacketNavigateQBook()
	{
	}
	
	@Override
	public iPacket onArrived(PacketNavigateQBook packet, MessageContext context)
	{
		if(context.side == Side.CLIENT)
			packet.clientHandle();
		return null;
	}
	
	@SideOnly(Side.CLIENT)
	public void clientHandle()
	{
		SimpleQuesting.proxy.openQuestingBook(Minecraft.getMinecraft().player, nav);
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setInteger("Type", nav == Navigation.LAST_NAVIGATION ? 0 : nav == Navigation.RESET_NAVIGATION ? 1 : 2);
		if(nbt.getInteger("Type") == 2)
			nav.writeToNBT(nbt);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		int type = nbt.getInteger("Type");
		if(type == 0)
			nav = Navigation.LAST_NAVIGATION;
		else if(type == 1)
			nav = Navigation.RESET_NAVIGATION;
		else
			nav = new Navigation(nbt);
	}
}