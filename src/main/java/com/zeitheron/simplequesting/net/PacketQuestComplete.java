package com.zeitheron.simplequesting.net;

import com.pengu.hammercore.net.packetAPI.iPacket;
import com.pengu.hammercore.net.packetAPI.iPacketListener;

import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketQuestComplete implements iPacket, iPacketListener<PacketQuestComplete, iPacket>
{
	
	
	@Override
	public iPacket onArrived(PacketQuestComplete packet, MessageContext context)
	{
		return null;
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
	}
}