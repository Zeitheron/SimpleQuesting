package com.zeitheron.simplequesting.net;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import com.zeitheron.hammercore.netv2.IV2Packet;
import com.zeitheron.hammercore.netv2.PacketContext;
import com.zeitheron.hammercore.netv2.transport.NetTransport;
import com.zeitheron.hammercore.netv2.transport.TransportSession;
import com.zeitheron.simplequesting.SimpleQuesting;
import com.zeitheron.simplequesting.book.reg.QuestLibrary;
import com.zeitheron.simplequesting.client.ClientLibLoad;

public class PacketRequestQuestLib implements IV2Packet
{
	static
	{
		IV2Packet.handle(PacketRequestQuestLib.class, () -> new PacketRequestQuestLib());
	}
	
	@Override
	public IV2Packet executeOnServer(PacketContext net)
	{
		QuestLibrary lib = SimpleQuesting.instance.SERVER_QUESTS;
		if(lib != null)
			try
			{
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				lib.save(new DataOutputStream(baos));
				byte[] data = baos.toByteArray();
				SimpleQuesting.LOG.info(net.getSender() + " requested quest library. Providing " + data.length + " bytes.");
				return NetTransport.builder().addData(data).setAcceptor(ClientLibLoad.class).build().createPacket();
			} catch(Throwable err)
			{
				err.printStackTrace();
			}
		return null;
	}
}