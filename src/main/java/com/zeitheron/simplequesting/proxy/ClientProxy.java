package com.zeitheron.simplequesting.proxy;

import com.zeitheron.simplequesting.book.Navigation;
import com.zeitheron.simplequesting.book.reg.QuestLibrary;
import com.zeitheron.simplequesting.client.ClientQuestInfo;
import com.zeitheron.simplequesting.client.gui.QBookGuiDispatcher;
import com.zeitheron.simplequesting.client.gui.editor.EBookGuiDispatcher;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class ClientProxy extends CommonProxy
{
	@Override
	public void init()
	{
		MinecraftForge.EVENT_BUS.register(new ClientQuestInfo());
	}
	
	@Override
	public void openQuestingBook(EntityPlayer player, Navigation path)
	{
		if(player.world.isRemote)
		{
			GuiScreen gui = QBookGuiDispatcher.fromNavigation(path);
			if(gui != null)
				Minecraft.getMinecraft().displayGuiScreen(gui);
		} else
			super.openQuestingBook(player, path);
	}
	
	@Override
	public void openEditorBook(EntityPlayer player, Navigation path)
	{
		if(player.world.isRemote)
		{
			GuiScreen gui = EBookGuiDispatcher.fromNavigation(path);
			if(gui != null)
				Minecraft.getMinecraft().displayGuiScreen(gui);
		} else
			super.openEditorBook(player, path);
	}
	
	@Override
	public QuestLibrary getQuestLibrary(World world)
	{
		if(world.isRemote)
			return ClientQuestInfo.library;
		return super.getQuestLibrary(world);
	}
	
	@Override
	public String getLang()
	{
		return Minecraft.getMinecraft().gameSettings.language;
	}
}