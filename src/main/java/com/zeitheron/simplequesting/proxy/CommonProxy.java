package com.zeitheron.simplequesting.proxy;

import com.pengu.hammercore.net.HCNetwork;
import com.zeitheron.simplequesting.SimpleQuesting;
import com.zeitheron.simplequesting.book.Navigation;
import com.zeitheron.simplequesting.book.reg.QuestLibrary;
import com.zeitheron.simplequesting.net.PacketNavigateEBook;
import com.zeitheron.simplequesting.net.PacketNavigateQBook;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.World;

public class CommonProxy
{
	public void init()
	{
		
	}
	
	public void openQuestingBook(EntityPlayer player, Navigation path)
	{
		if(!player.world.isRemote && player instanceof EntityPlayerMP)
			HCNetwork.manager.sendTo(new PacketNavigateQBook(path), (EntityPlayerMP) player);
	}
	
	public void openEditorBook(EntityPlayer player, Navigation path)
	{
		if(!player.world.isRemote && player instanceof EntityPlayerMP)
			HCNetwork.manager.sendTo(new PacketNavigateEBook(path), (EntityPlayerMP) player);
	}
	
	public QuestLibrary getQuestLibrary(EntityPlayer player)
	{
		return getQuestLibrary(player.world);
	}
	
	public QuestLibrary getQuestLibrary(World world)
	{
		return SimpleQuesting.instance.SERVER_QUESTS;
	}
	
	public String getLang()
	{
		return "en_us";
	}
}