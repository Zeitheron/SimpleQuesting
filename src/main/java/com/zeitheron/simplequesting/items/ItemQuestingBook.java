package com.zeitheron.simplequesting.items;

import com.pengu.hammercore.common.items.iCustomEnchantColorItem;
import com.zeitheron.simplequesting.SimpleQuesting;
import com.zeitheron.simplequesting.book.Navigation;
import com.zeitheron.simplequesting.init.ItemsSQ;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class ItemQuestingBook extends Item implements iCustomEnchantColorItem
{
	public int enchCol = -1;
	public boolean hasGlow = false;
	
	public ItemQuestingBook(String name)
	{
		this(name, -1);
		hasGlow = false;
	}
	
	public ItemQuestingBook(String name, int enchCol)
	{
		setUnlocalizedName(name);
		this.enchCol = enchCol;
		hasGlow = true;
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn)
	{
		if(worldIn.isRemote)
		{
			if(this == ItemsSQ.EDITOR_BOOK)
				SimpleQuesting.proxy.openEditorBook(playerIn, Navigation.LAST_NAVIGATION);
			else
				SimpleQuesting.proxy.openQuestingBook(playerIn, Navigation.LAST_NAVIGATION);
		}
		return new ActionResult<ItemStack>(EnumActionResult.PASS, playerIn.getHeldItem(handIn));
	}
	
	@Override
	public boolean hasEffect(ItemStack stack)
	{
		return hasGlow;
	}
	
	@Override
	public int getEnchantEffectColor(ItemStack stack)
	{
		return enchCol;
	}
}