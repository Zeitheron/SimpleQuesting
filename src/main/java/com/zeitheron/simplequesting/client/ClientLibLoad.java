package com.zeitheron.simplequesting.client;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.zeitheron.hammercore.netv2.transport.ITransportAcceptor;
import com.zeitheron.simplequesting.SimpleQuesting;
import com.zeitheron.simplequesting.book.reg.QuestLibrary;

public class ClientLibLoad implements ITransportAcceptor
{
	@Override
	public void read(InputStream readable, int length)
	{
		SimpleQuesting.LOG.info("Accepting quests from server... (" + length + " bytes)");
		
		DataInputStream din = new DataInputStream(readable);
		QuestLibrary ql = new QuestLibrary();
		try
		{
			ql.load(din);
			ClientQuestInfo.library = ql;
		} catch(IOException e)
		{
			e.printStackTrace();
		}
		
		if(ClientQuestInfo.library != null)
			SimpleQuesting.LOG.info("Quests received!");
		else
		{
			SimpleQuesting.LOG.info("Quests weren't received for some reason!");
			ClientQuestInfo.library = new QuestLibrary();
		}
	}
}