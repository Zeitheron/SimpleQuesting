package com.zeitheron.simplequesting.client;

import com.zeitheron.hammercore.netv2.HCV2Net;
import com.zeitheron.simplequesting.SimpleQuesting;
import com.zeitheron.simplequesting.book.reg.QuestLibrary;
import com.zeitheron.simplequesting.net.PacketRequestQuestLib;

import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent.ClientConnectedToServerEvent;

public class ClientQuestInfo
{
	public static QuestLibrary library;
	
	public boolean inWorld = false;
	
	@SubscribeEvent
	public void renderWorld(RenderWorldLastEvent w)
	{
		if(!inWorld)
		{
			inWorld = true;
			HCV2Net.INSTANCE.sendToServer(new PacketRequestQuestLib());
			SimpleQuesting.LOG.info("Requesting quests...");
		}
	}
	
	@SubscribeEvent
	public void clientConnect(ClientConnectedToServerEvent e)
	{
		inWorld = false;
	}
}