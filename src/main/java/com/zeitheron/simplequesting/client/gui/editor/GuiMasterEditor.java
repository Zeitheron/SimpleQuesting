package com.zeitheron.simplequesting.client.gui.editor;

import com.pengu.hammercore.core.gui.GuiCentered;

public class GuiMasterEditor extends GuiCentered
{
	@Override
	public void initGui()
	{
		super.initGui();
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		drawDefaultBackground();
	}
}