package com.zeitheron.simplequesting.book.reg;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class QuestLoader
{
	public static QuestLibrary loadLibrary(InputStream input) throws IOException
	{
		DataInputStream din = new DataInputStream(input);
		QuestLibrary lib = new QuestLibrary();
		lib.load(din);
		return lib;
	}
	
	public static void saveLibrary(OutputStream output, QuestLibrary lib) throws IOException
	{
		DataOutputStream dout = new DataOutputStream(output);
		lib.save(dout);
		dout.flush();
		output.flush();
	}
}