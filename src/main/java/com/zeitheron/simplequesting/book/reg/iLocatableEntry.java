package com.zeitheron.simplequesting.book.reg;

public interface iLocatableEntry extends iQuestEntry
{
	int getX();
	
	int getY();
}