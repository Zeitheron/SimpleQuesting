package com.zeitheron.simplequesting.book.reg;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.zeitheron.simplequesting.utils.TranslatableString;

public class QuestCategory implements iQuestEntry
{
	private TranslatableString name = new TranslatableString();
	public final List<iQuestEntry> childs = new ArrayList<>();
	public String id;
	
	QuestCategory()
	{
	}
	
	public QuestCategory(String id)
	{
		this.id = id;
	}
	
	@Override
	public TranslatableString getDisplayName()
	{
		return name;
	}
	
	@Override
	public void load(DataInputStream in) throws IOException
	{
		this.name = new TranslatableString();
		this.name.load(in);
		
		id = IO.readString(in);
		
		int left = in.readShort();
		
		for(int i = 0; i < left; ++i)
			try
			{
				childs.add((iQuestEntry) IO.readFlushable(in));
			} catch(ClassNotFoundException | ClassCastException e)
			{
				throw new IOException(e);
			}
	}
	
	@Override
	public void save(DataOutputStream out) throws IOException
	{
		this.name.save(out);
		
		IO.writeString(out, id);
		
		out.writeShort(childs.size());
		
		for(iQuestEntry e : childs)
			IO.writeFlushable(out, e);
	}

	@Override
	public String getId()
	{
		return id;
	}
}