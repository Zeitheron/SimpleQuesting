package com.zeitheron.simplequesting.book.reg;

import com.zeitheron.simplequesting.utils.TranslatableString;
import com.zeitheron.simplequesting.utils.iDataFlushable;

public interface iQuestEntry extends iDataFlushable
{
	TranslatableString getDisplayName();
	
	String getId();
}