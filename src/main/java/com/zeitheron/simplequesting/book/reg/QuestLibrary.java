package com.zeitheron.simplequesting.book.reg;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.zeitheron.simplequesting.utils.iDataFlushable;
import com.zeitheron.simplequesting.utils.iDataFlushable.IO;

public class QuestLibrary implements iDataFlushable
{
	public List<QuestCategory> categories = new ArrayList<>();
	
	@Override
	public void load(DataInputStream in) throws IOException
	{
		int left = in.readShort();
		
		for(int i = 0; i < left; ++i)
			try
			{
				categories.add((QuestCategory) IO.readFlushable(in));
			} catch(ClassNotFoundException | ClassCastException e)
			{
				throw new IOException(e);
			}
	}
	
	@Override
	public void save(DataOutputStream out) throws IOException
	{
		out.writeShort(categories.size());
		
		for(iQuestEntry e : categories)
			IO.writeFlushable(out, e);
	}
}