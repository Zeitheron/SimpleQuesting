package com.zeitheron.simplequesting.book;

import net.minecraft.nbt.NBTTagCompound;

public class Navigation
{
	public static final Navigation LAST_NAVIGATION = new Navigation();
	public static final Navigation RESET_NAVIGATION = null;
	
	public String category = null;
	public Quest quest = null;
	
	public Navigation()
	{
	}
	
	public Navigation(NBTTagCompound nbt)
	{
		if(nbt.getBoolean("CatOpen"))
			setCategory(nbt.getString("Category"));
		if(nbt.getBoolean("QuestOpen"))
			setQuest(new Quest(nbt, "Quest"));
	}
	
	public Navigation setCategory(String category)
	{
		this.category = category;
		return this;
	}
	
	public Navigation setQuest(Quest quest)
	{
		this.quest = quest;
		return this;
	}
	
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setBoolean("CatOpen", category != null);
		nbt.setBoolean("QuestOpen", quest != null);
		if(quest != null)
			quest.writeToNBT(nbt, "Quest");
		if(category != null)
			nbt.setString("Category", category);
	}
	
	public static class Quest
	{
		public final String id;
		public int page;
		
		public Quest(String id)
		{
			this.id = id;
		}
		
		public Quest(NBTTagCompound nbt, String tag)
		{
			this(nbt.getString(tag + "Id"));
			setPage(nbt.getByte(tag + "Page"));
		}
		
		public Quest setPage(int page)
		{
			this.page = page;
			return this;
		}
		
		public void writeToNBT(NBTTagCompound nbt, String tag)
		{
			nbt.setString(tag + "Id", id);
			nbt.setByte(tag + "Page", (byte) page);
		}
	}
}