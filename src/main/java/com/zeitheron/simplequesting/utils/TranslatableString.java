package com.zeitheron.simplequesting.utils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.zeitheron.simplequesting.SimpleQuesting;

import java.util.Set;

public class TranslatableString implements iDataFlushable
{
	private final Map<String, String> langs = new HashMap<>();
	
	public TranslatableString(String en_us, Map<String, String> translations)
	{
		langs.putAll(translations);
		langs.put("en_us", en_us);
	}
	
	public TranslatableString(String val)
	{
		langs.put("en_us", val);
	}
	
	public TranslatableString()
	{
	}
	
	public boolean translate(String lang, String val)
	{
		if(!langs.containsKey(lang.toLowerCase()))
			return langs.put(lang, val) == null;
		return false;
	}
	
	@Override
	public void load(DataInputStream in) throws IOException
	{
		langs.clear();
		
		int fo = in.readShort();
		for(int i = 0; i < fo; ++i)
		{
			String lang = IO.readStringB(in);
			String val = IO.readString(in);
			
			langs.put(lang, val);
		}
	}
	
	@Override
	public void save(DataOutputStream out) throws IOException
	{
		Set<Entry<String, String>> strs = langs.entrySet();
		
		out.writeShort(strs.size());
		for(Entry<String, String> e : strs)
		{
			IO.writeStringB(out, e.getKey());
			IO.writeString(out, e.getValue());
		}
	}
	
	@Override
	public String toString()
	{
		return langs.getOrDefault(SimpleQuesting.proxy.getLang().toLowerCase(), langs.get("en_us"));
	}
}