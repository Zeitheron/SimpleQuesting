package com.zeitheron.simplequesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.pengu.hammercore.common.SimpleRegistration;
import com.zeitheron.simplequesting.book.reg.QuestLibrary;
import com.zeitheron.simplequesting.book.reg.QuestLoader;
import com.zeitheron.simplequesting.init.ItemsSQ;
import com.zeitheron.simplequesting.proxy.CommonProxy;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLFingerprintViolationEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;
import net.minecraftforge.fml.common.eventhandler.EventBus;

@Mod(modid = "simplequesting", version = "@VERSION@", name = "Simple Questing", dependencies = "required-after:hammercore", certificateFingerprint = "4d7b29cd19124e986da685107d16ce4b49bc0a97")
public class SimpleQuesting
{
	@SidedProxy(clientSide = "com.zeitheron.simplequesting.proxy.ClientProxy", serverSide = "com.zeitheron.simplequesting.proxy.CommonProxy")
	public static CommonProxy proxy;
	@Instance
	public static SimpleQuesting instance;
	public static final EventBus SQ_BUS = new EventBus();
	public static final Logger LOG = LogManager.getLogger("SimpleQuesting");
	public File configDir;
	public QuestLibrary SERVER_QUESTS = null;
	
	@EventHandler
	public void certificateViolation(FMLFingerprintViolationEvent e)
	{
		LOG.warn("*****************************");
		LOG.warn("WARNING: Somebody has been tampering with SimpleQuesting jar!");
		LOG.warn("It is highly recommended that you redownload mod from https://minecraft.curseforge.com/projects/291089 !");
		LOG.warn("*****************************");
	}
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		configDir = new File("config", "SimpleQuesting");
		if(!configDir.isDirectory())
			configDir.mkdirs();
		SimpleRegistration.registerFieldItemsFrom(ItemsSQ.class, "simplequesting", CreativeTabs.MISC);
	}
	
	@EventHandler
	public void init(FMLInitializationEvent e)
	{
		proxy.init();
	}
	
	@EventHandler
	public void serverStarting(FMLServerStartingEvent e)
	{
		File quests = new File(configDir, "QuestLibrary.ql");
		if(quests.isFile())
		{
			LOG.info("Loading Quest Library...");
			
			FileInputStream inp = null;
			
			try
			{
				inp = new FileInputStream(quests);
				this.SERVER_QUESTS = QuestLoader.loadLibrary(inp);
				inp.close();
				
				LOG.info("Quests Loaded!");
			} catch(Throwable err)
			{
				this.SERVER_QUESTS = new QuestLibrary();
				if(inp != null)
					try
					{
						inp.close();
					} catch(Throwable err2)
					{
						// Shouldn't happen
					}
				LOG.error("Quest Library is corrupted!");
				quests.renameTo(new File(configDir, "QuestLibrary_errored.ql"));
				err.printStackTrace();
				try(PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(new File(configDir, "QuestLibrary_err.i.txt")))))
				{
					err.printStackTrace(pw);
				} catch(IOException e1)
				{
					LOG.error("Failed to save stack trace of the error!");
					e1.printStackTrace();
				}
			}
		} else
		{
			this.SERVER_QUESTS = new QuestLibrary();
			LOG.info("QuestLibrary file not found! It will be created after server shutdown.");
		}
	}
	
	@EventHandler
	public void serverStop(FMLServerStoppingEvent e)
	{
		File quests = new File(configDir, "QuestLibrary.ql");
		File bak = new File(configDir, "QuestLibrary.ql.bak");
		if(quests.isFile())
		{
			if(bak.isFile())
				bak.delete();
			quests.renameTo(bak);
			quests.delete();
		}
		
		LOG.info("Saving QuestLibrary...");
		
		FileOutputStream out = null;
		
		try
		{
			out = new FileOutputStream(quests);
			QuestLoader.saveLibrary(out, this.SERVER_QUESTS);
			out.close();
			this.SERVER_QUESTS = null;
			LOG.info("Quests Saved!");
		} catch(Throwable err)
		{
			if(out != null)
				try
				{
					out.close();
				} catch(Throwable err2)
				{
					// Shouldn't happen
				}
			LOG.error("Quest Library cannot be saved!");
			err.printStackTrace();
			try(PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(new File(configDir, "QuestLibrary_err.o.txt")))))
			{
				err.printStackTrace(pw);
			} catch(IOException e1)
			{
				LOG.error("Failed to save stack trace of the error!");
				e1.printStackTrace();
			}
		}
	}
}